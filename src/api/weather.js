import axios from 'axios'

export class Weather {
  constructor () {
    this.API_KEY = 'b03ffb4b0cfbbbdd357352f67eae557e'
    this.API_URL = 'http://api.openweathermap.org/data/2.5'
  }

  /**
   * Get city weather by name or city id
   *
   * @param params
   *  Example: q=Kiev,ua or id=703448
   *
   * @returns {AxiosPromise}
   */
  getForecastWeather (params) {
    params.APPID = this.API_KEY
    params = params ? this.objectToUrl(params) : params
    return axios(
      {
        url: `${this.API_URL}/forecast/?${params}`,
        method: 'GET'
      }
    )
  }

  /**
   * Find cities by Name
   *
   * @param params
   * @returns {AxiosPromise}
   */
  getFindWeather (params) {
    params.APPID = this.API_KEY
    params = params ? this.objectToUrl(params) : params

    return axios(
      {
        url: `${this.API_URL}/find/?${params}`,
        method: 'GET'
      }
    )
  }

  /**
   * Generate url by params
   *
   * @param obj
   * @returns {string}
   */
  objectToUrl (obj) {
    return Object.entries(obj).map(([key, val]) => `${key}=${val}`).join('&')
  }
}
