const state = {
  list: [
  ]
}

const mutations = {
  setCitiesList: (state, list) => {
    state.list = list
  }
}

const getters = {
  getCityByID (state) {
    return id => {
      let city = state.list.find(city => city.id === id)

      if (typeof city === 'undefined') {
        city = null
      }

      return city
    }
  }
}

const actions = {
  deleteCity: (context, id) => {
    let list = context.state.list.filter(city => city.id !== id)

    localStorage.setItem('cities', JSON.stringify(list))
    context.commit('setCitiesList', list)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
