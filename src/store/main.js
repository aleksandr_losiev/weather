const state = {
  isOpenSidebar: false
}

const mutations = {
  setIsOpenSidebar: (state, status) => {
    state.isOpenSidebar = status
  }
}

const actions = {
  toggleSidebar: (context) => {
    context.commit('setIsOpenSidebar', !context.state.isOpenSidebar)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
