import { Weather } from '../api/weather'

const weather = new Weather()

const state = {
  list: []
}

const mutations = {
  setListItem: (state, list) => {
    state.list = list
  }
}

const actions = {
  setListItem: async (context, data) => {
    let cities = context.rootState.cities.list

    cities.map(city => {
      let params = {
        ...data,
        id: city.id
      }

      weather.getForecastWeather(params).then(result => {
        if (result.status === 200) {
          let list = context.state.list
          let city = list.filter(item => item.city.id !== result.data.city.id)

          context.commit('setListItem', [...city, result.data])
        }
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
