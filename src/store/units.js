const state = {
  list: [
    {
      id: 1,
      type: 'standard',
      name: 'Kelvin',
      icon: '&deg;K'
    },
    {
      id: 2,
      type: 'metric',
      name: 'Celsius',
      icon: '&deg;C'
    },
    {
      id: 3,
      type: 'imperial',
      name: 'Fahrenheit',
      icon: '&deg;F'
    }
  ],
  activeID: 2
}

const mutations = {
  setActiveID: (state, id) => {
    state.activeID = id
  }
}

const getters = {
  getActiveUnit (state) {
    return state.list.find(item => item.id === state.activeID)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters
}
