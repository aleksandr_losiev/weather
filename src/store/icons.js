let state = {
  list: [
    {
      codes: [800],
      iconClassDay: 'flaticon-013-sun',
      iconClassNight: 'flaticon-011-night',
      name: 'Clear sky'
    },
    {
      codes: [801],
      iconClassDay: 'flaticon-049-cloudy',
      iconClassNight: 'flaticon-050-cloudy',
      name: 'Few clouds'
    },
    {
      codes: [802, 803],
      iconClassDay: 'flaticon-002-windy',
      iconClassNight: 'flaticon-003-windy',
      name: 'Scattered clouds'
    },
    {
      codes: [804],
      iconClassDay: 'flaticon-023-storm',
      iconClassNight: 'flaticon-023-storm',
      name: 'Scattered clouds'
    },
    {
      codes: [300, 301, 302, 310, 311, 312, 313, 314, 321, 520, 521, 522, 531],
      iconClassDay: 'flaticon-034-hail',
      iconClassNight: 'flaticon-034-hail',
      name: 'Shower rain'
    },
    {
      codes: [500, 501, 502, 503, 504],
      iconClassDay: 'flaticon-001-rainy-day',
      iconClassNight: 'flaticon-012-rainy',
      name: 'Rain'
    },
    {
      codes: [200, 201, 202, 210, 211],
      iconClassDay: 'flaticon-047-storm',
      iconClassNight: 'flaticon-048-storm',
      name: 'Thunderstorm'
    },
    {
      codes: [212, 221, 230, 231, 232],
      iconClassDay: 'flaticon-045-storm',
      iconClassNight: 'flaticon-045-storm',
      name: 'Heavy thunderstorm'
    },
    {
      codes: [600, 601, 602],
      iconClassDay: 'flaticon-005-snowym',
      iconClassNight: 'flaticon-006-snowy',
      name: 'Snow'
    },
    {
      codes: [611, 612, 613, 614, 615, 616],
      iconClassDay: 'flaticon-009-snowym',
      iconClassNight: 'flaticon-010-snowy',
      name: 'Snow sleet'
    },
    {
      codes: [620, 621, 622],
      iconClassDay: 'flaticon-018-snowflake',
      iconClassNight: 'flaticon-018-snowflake',
      name: 'Heavy shower snow'
    },
    {
      codes: [701, 711, 721, 731, 741, 751, 761, 762, 771],
      iconClassDay: 'flaticon-039-weathercock',
      iconClassNight: 'flaticon-039-weathercock',
      name: 'Mist'
    },
    {
      codes: [781],
      iconClassDay: 'flaticon-015-tornado',
      iconClassNight: 'flaticon-015-tornado',
      name: 'Tornado'
    }
  ],
  defaultCode: 800
}

const getters = {
  getIconByID (state) {
    return (id, hours = null) => {
      let iconData = state.list.find(item => {
        return item.codes.includes(id)
      })

      const hoursData = hours === null ? new Date().getHours() : hours

      return hoursData > 5 && hoursData < 19 ? iconData.iconClassDay : iconData.iconClassNight
    }
  }
}

export default {
  namespaced: true,
  state,
  getters
}
