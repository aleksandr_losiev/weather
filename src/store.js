import Vue from 'vue'
import Vuex from 'vuex'

import cities from './store/cities'
import units from './store/units'
import weather from './store/weather'
import icons from './store/icons'
import main from './store/main'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  modules: {
    cities,
    units,
    weather,
    icons,
    main
  }
})
